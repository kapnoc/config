# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

set -o vi

# Stop Ctrl-S & Ctrl-Q signals
stty -ixon

# enter command in history instantly, not just when bash exits
export PROMPT_COMMAND='history -a; history -r'

PS1="$(echo -ne '\[\e[1;32m\]\$>\[\e[0m\]') "

export EDITOR='vi'
export PAGER='less -R'

# colour aliases
alias ls='ls --color=auto'
alias grep='grep --colour=auto'
alias tree='tree -C'

# common use aliases
alias ..='cd ..'
alias cl='clear'
alias ll='ls -l'
alias la='ls -la'
alias ne='emacs -nw'
alias vv='nvim'

alias aled="startx -- vt1"

# GIT
alias ga='git add'
alias gc='git commit'
alias gd='git diff'
alias gdm='git diff master'
alias gl='git llog'
alias gpl='git pull'
alias gps='git push'
alias gs='git status'


# less used aliases
wttr()
{
	curl -H "Accept-Language: ${LANG%_*}" wttr.in/"${1:-Nancy}"
}

export PATH="$PATH:/home/kapnoc/bin"


export JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk"
export ANDROID_NDK="/home/kapnoc/Android/Sdk/ndk/21.0.6113669"
export ANDROID_SDK="/home/kapnoc/Android/Sdk"
export PATH=$PATH:$JAVA_HOME/bin




