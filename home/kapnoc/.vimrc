
call plug#begin()

Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" C++
Plug 'bfrg/vim-cpp-modern'

" HTML
Plug 'mattn/emmet-vim'

call plug#end()

let g:UltiSnipsExpandTrigger="<tab>"


