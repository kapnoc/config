## dnf

```sh
sudo dnf install \
	dmenu make gtest-devel openssl-devel g++ git \
	nvi cmake keepassxc ImageMagick neovim chromium \
	python3-neovim htop
```


## snap

```sh
sudo dnf install snapd
sudo ln -s /var/lib/snapd/snap /snap
sudo systemctl enable snapd
sudo systemctl start snapd
snap install spotify discord
```
